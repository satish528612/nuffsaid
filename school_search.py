# importing csv module
import csv
import time

# csv file name
filename = "school_data.csv"

# initializing the titles and rows list
rows = None
school_cache = None
city_cache = None
state_cache = None


def add_to_list(map, key_list, value):
    key_list = key_list.lower().replace('school', '').replace('-', '').replace(',', '')

    temp = [key_list] + key_list.split()
    for x in temp:
        listTemp = map.get(x, set())
        listTemp.add(value)
        map[x] = listTemp


def read_file():
    # resetting global variables
    global rows
    global school_cache
    global city_cache
    global state_cache
    rows = []
    school_cache = {}
    city_cache = {}
    state_cache = {}

    # reading csv file
    with open(filename, encoding="utf8", errors='ignore') as csvfile:
        # creating a csv reader object
        csvreader = csv.reader(csvfile)

        # skipping first row titles
        next(csvreader)

        # extracting each data row one by one
        for row in csvreader:
            rows.append(row)

        for i in range(len(rows)):
            add_to_list(school_cache, rows[i][3], i)
            add_to_list(city_cache, rows[i][4], i)
            add_to_list(state_cache, rows[i][5], i)


def reverse_dict_with_count(results):
    new_dict = {}
    for k, v in results.items():
        temp_list = new_dict.get(len(v), [])
        temp_list.append(k)
        new_dict[len(v)] = temp_list
    return new_dict


def update_result(cache, results, search_term_splitted):
    if search_term_splitted in cache:
        for x in cache.get(search_term_splitted):
            temp_list = results.get(x, set())
            temp_list.add(search_term_splitted)
            results[x] = temp_list

def print_row_by_list_of_indexes(list_of_indexes, counter=1):
    for i in list_of_indexes:
        print('%d. %s\n%s, %s' % (counter, rows[i][3], rows[i][4], rows[i][5]))
        counter += 1
    return counter

def search_schools(search_term):
    read_file()
    start_time = time.time()
    search_term = search_term.lower().replace('school', '').strip()

    # if search is for "school", return all rows
    if search_term == '':
        print('Results for "%s" (search took: %.3fs)' % (search_term, time.time() - start_time))
        print_row_by_list_of_indexes(range(len(rows)))
        return

    search_list = search_term.split()
    results = {}

    for search_term_splitted in search_list:
        update_result(city_cache, results, search_term_splitted)
        update_result(state_cache, results, search_term_splitted)
        update_result(school_cache, results, search_term_splitted)

    results2 = reverse_dict_with_count(results)

    print('Results for "%s" (search took: %.3fs)' % (search_term, time.time() - start_time))

    counter = 1
    for ii in sorted(results2.keys(), reverse=True):
        counter = print_row_by_list_of_indexes(results2.get(ii), counter)


if __name__ == '__main__':
    search_schools('elementary school highland park')
    # search_schools('jefferson belleville')
    # search_schools('riverside school 44')
    # search_schools('granada charter school')
    # search_schools('foley high alabama')
    # search_schools('KUSKOKWIM')
    # search_schools('school')


