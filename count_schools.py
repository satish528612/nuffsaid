# importing csv module
import csv

# csv file name
filename = "school_data.csv"

# initializing the titles and rows list
fields = []
rows = []


def read_file():
    # reading csv file
    with open(filename, encoding="utf8", errors='ignore') as csvfile:
        # creating a csv reader object
        csvreader = csv.reader(csvfile)

        # skipping first row titles
        next(csvreader)

        # extracting each data row one by one
        for row in csvreader:
            rows.append(row)

        # get total number of rows
        print("Total Schools: %d" % (len(rows)))


# Count frequency by passing index of column start 0
def count_by_attribute(index):
    count_by_attribute_dict = {}
    for row in rows:
        # parsing each column of a row
        count_by_attribute_dict[row[index]] = count_by_attribute_dict.get(row[index], 0) + 1
    for key, value in count_by_attribute_dict.items():
        print(key + " " + str(value))


# Count frequency by passing index of column start 0
def get_max_count_by_attribute(index):
    count_by_attribute_dict = {}
    maxV = -1
    maxKey = None
    for row in rows:
        count_by_attribute_dict[row[index]] = count_by_attribute_dict.get(row[index], 0) + 1
        if count_by_attribute_dict[row[index]] > maxV:
            maxV = count_by_attribute_dict[row[index]]
            maxKey = row[index]
    return maxKey, maxV


def get_unique_cols_count(index):
    count_by_attribute_dict = set()
    for row in rows:
        count_by_attribute_dict.add(row[index])
    return len(count_by_attribute_dict)


def print_counts():
    read_file()
    print('Schools by State:')
    count_by_attribute(5)
    print('Schools by Metro-centric locale:')
    count_by_attribute(8)
    maxKey, maxValue = get_max_count_by_attribute(4)
    print('City with most schools: %s (%d schools)' % (maxKey, maxValue))
    print('Unique cities with at least one school: %d' % get_unique_cols_count(4))


if __name__ == '__main__':
    print_counts()
